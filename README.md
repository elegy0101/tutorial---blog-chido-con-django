## Blog Chido en Django :sunglasses:

Este repositorio contiene los archivos base y la documentación necesaria para iniciar en el desarollo de aplicaciones web con Django. 

Puedes consultar la [Wiki](https://gitlab.com/elegy0101/tutorial---blog-chido-con-django/wikis/Introducci%C3%B3n) para revisar su contenido y darle marcha al tutorial :computer:

Si deseas contribuir en seguida vienen las instrucciones sobre como hacerlo.


## Como contribuir :heart:

Este tutorial está bajo una licencia [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es). Todos son libres de agregar, editar y corregir el tutorial.


## Como editar

El código fuente del tutorial [está alojado en GitLab](). El flujo de trabajo de [Merge Request de GitLab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) se utiliza para aceptar y revisar cambios.

El tutorial está escrito en [Markdown Mark Up Language](https://docs.gitlab.com/ee/user/markdown.html).

Puedes encontrar discusiones sobre el contenido del tutorial en el [rastreador de issues de GitLab](https://gitlab.com/elegy0101/tutorial---blog-chido-con-django/issues).


## Primeros pasos y requisitos previos

Para contribuir al tutorial se necesita lo siguiente para comenzar: 

* Una cuenta de GitLab
* En el caso de ediciones complejas, familiarizarse con los conceptos básicos de la línea de comandos de Git.


## Haz fork al repositorio 

Primero haz fork al repositorio de [Tutorial- Blog chido con Django](https://gitlab.com/elegy0101/tutorial---blog-chido-con-django) a tu cuenta personal de GitLab:

![Hacer Fork](img/1.jpg)


## Hacer merge request

Para realizar un merge request selecciona una de la opciones como aparece a continuación:

 
![Merge Request](img/Selección_044.jpg)
 
 
## Mayor información y ayuda

GitLab tiene una excelente [documentación](https://docs.gitlab.com/). ¡Puedes consultarla si necesita ayuda!

Para más preguntas, puedes contactarme desde nep.ceballos@gmail.com


¡Gracias por Leer! :relieved: :v: 






